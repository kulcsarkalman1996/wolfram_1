<?php
namespace App\Libraries;

use Phpml\Math\Matrix;
use App\Exceptions\MatrixNotSquare;
use App\Exceptions\MatrixIsSingular;

class LUP_NEW
{
    protected $matrix;
    protected $rows;
    protected $columns;
    protected $solution;
    protected $matrox_obj;

    public function __construct(Matrix $matrix)
    {
        $this->matrix_obj = $matrix;
        $this->matrix = $matrix->toArray();
        $this->rows = $matrix->getRows();
        $this->columns = $matrix->getColumns();
    }

    public function handle()
    {
        $this->calcLUP($this->matrix);
        return $this->solution;
    }

    
    protected function calcLUP(array $matrix)
    {
        for ($i = 0; $i < $this->rows; $i++) 
            for ($j = 0; $j < $this->rows; $j++) 
                if ($i == $j)
                    $P[$i][$j] = 1;
                else
                    $P[$i][$j] = 0;
        $A = $matrix;
        $exchanges = 0;
        for ($i = 0; $i < $this->rows; $i++) {
            //start pivot section
            $Umax = 0;
            for ($r=$i; $r<$this->rows; $r++) {
                $Uii=$A[$r][$i];
                $q = 0;
                while ($q<$i) {
                    $Uii -= $A[$r][$q]*$A[$q][$r];
                    $q++;
                } 
                if (abs($Uii)>$Umax) {
                    $Umax = abs($Uii);
                    $row = $r;
                }
            }
            if ($i!=$row) {//swap rows
                $exchanges++;
                for ($q=0; $q<$this->rows; $q++) {
                    $tmp = $P[$i][$q];
                    $P[$i][$q]=$P[$row][$q];
                    $P[$row][$q]=$tmp;
                    $tmp = $A[$i][$q];
                    $A[$i][$q]=$A[$row][$q];
                    $A[$row][$q]=$tmp;
                } 
            }
        //end pivot section

                $j = $i;
                while ($j<$this->rows) { //Determine U across row i
                    $q = 0;
                while ($q<$i) {
                    $A[$i][$j] -= $A[$i][$q]*$A[$q][$j];
                    $q++;
                } 
                $j++;
                }
                $j = $i+1;
                while ($j<$this->rows) { //Determine L down column i
                    $q = 0;
                while ($q<$i) {
                    $A[$j][$i] -= $A[$j][$q]*$A[$q][$i];
                    $q++;
                } 
                $A[$j][$i] = $A[$j][$i]/$A[$i][$i];
                $j++;
            }
        }
        // $LU = (new LUdecomposition(new Matrix($matrix)))->handle();
        $this->solution = [
            'LU' => $A,
            // 'L' => $LU['Lower_Matrix'],
            // 'U' => $LU['Upper_Matrix'],
            'P' => $P
        ];
    }
}