<?php
namespace App\Libraries;

use Phpml\Math\Matrix;
use App\Exceptions\MatrixNotSquare;
use App\Exceptions\MatrixIsSingular;

class LUPdecomposition
{
    protected $matrix;
    protected $rows;
    protected $columns;
    protected $solution;
    protected $matrox_obj;
    protected $b;

    public function __construct(Matrix $matrix)
    {
        $this->rows = $matrix->getRows();
        $this->columns = $matrix->getColumns();
        $this->matrix_obj = $matrix;
        $this->matrix = $this->getMatrices($matrix->toArray());
    }

    protected function getMatrices($matrix) 
    {
        if ($this->columns - $this->rows == 1) {
            for ($i = 0; $i < $this->rows; $i++){
                for ($j = 0; $j < $this->columns-1; $j++) {
                    if ($j == $this->columns - 2)
                        $this->b[$i] = $matrix[$i][$this->columns-1];
                    $var[$i][$j] = $matrix[$i][$j];
                }
            }
            return $var;
        }
        return $matrix;
    }

    public function handle()
    {
        $this->calcLUP($this->matrix);
        return $this->solution;
    }

    
    protected function calcLUP(array $matrix)
    {
        $swapped = $this->matrix_obj->toArray();
        for ($i = 0; $i < $this->rows; $i++)
            for ($j = 0; $j < $this->rows; $j++)
                $_P[$i][$j] = 0;

        for ($i = 0; $i < $this->rows; $i++) 
            $P[$i] = $i;
        
        for ($i = 0; $i < $this->rows - 1; $i++) {

            $maxPivot = $i;
            for ($k = $i + 1; $k < $this->rows; $k++) {
                if (abs($matrix[$k][$i]) > abs($matrix[$i][$i]))
                    $maxPivot = $k;
            }
            
            if ($maxPivot !== $i) {
                $temp = $P[$maxPivot];
                $P[$maxPivot] = $P[$i];
                $P[$i] = $temp;

                for ($k = $i; $k < $this->rows; $k++) {
                    $temp = $matrix[$i][$k];
                    $matrix[$i][$k] = $matrix[$maxPivot][$k];
                    $matrix[$maxPivot][$k] = $temp;   
                }

                if ($this->columns - $this->rows == 1) {
                    for ($k = $i; $k <= $this->rows; $k++) {
                        $temp = $swapped[$i][$k];
                        $swapped[$i][$k] = $swapped[$maxPivot][$k];
                        $swapped[$maxPivot][$k] = $temp;    
                    }
                }
            }

            // Gaussian elimination
            for ($k = $i + 1; $k < $this->rows; $k++) { // iterate down rows
                // lower triangle factor is not zeroed out, keep it for L
                $matrix[$k][$i] = $matrix[$k][$i] / $matrix[$i][$i]; // denominator is really for the pivot row elements
        
                for ($j = $i + 1; $j < $this->rows; $j++) // iterate across rows
                // subtract off lower triangle factor times pivot row
                $matrix[$k][$j] = $matrix[$k][$j] - $matrix[$i][$j] * $matrix[$k][$i];
            }
        }
        for ($i = 0; $i < $this->rows; $i++) {
            $_P[$i][$P[$i]] = 1;
            $x[$i] = 0;
            $z[$i] = 0;
        }
    
        // $Lower_upper = (new LUdecomposition($this->matrix_obj))->handle();
        $Lower_upper = (new LUdecomposition(new Matrix($swapped)))->handle();
                
        $this->solution = [
            'input' => $this->matrix_obj->toArray(),
            'LU'    => $this->round($matrix, $this->rows, $this->rows),
            'P'     => $_P,
            'X'     => $Lower_upper['X'],
            'Y'     => $Lower_upper['Y']
        ];
    }

    protected function round(array $matrix, int $n, int $m)
    {
        for ($i = 0; $i < $n; $i++)
            for ($j = 0; $j < $m; $j++)
                $matrix[$i][$j] = round($matrix[$i][$j], 2);
        return $matrix;
    }
}