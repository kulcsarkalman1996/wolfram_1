<?php

namespace App\Libraries;

use Phpml\Math\Matrix;

class GaussJordan
{
    protected $matrix;
    protected $rows;
    protected $columns;
    protected $solution;
    protected $return_data;

    public function __construct(Matrix $matrix)
    {
        $this->matrix = $matrix->toArray();
        $this->rows = $matrix->getRows();
        $this->columns = $matrix->getColumns();
    }

    public function handle()
    {
        $this->calcDiagonalMatrix($this->matrix);
        return $this->return_data = [
            'solution'  => $this->solution,
            'input'     => $this->matrix
        ];
    }

    protected function calcDiagonalMatrix(array $matrix)
    {
        for ($j = 0; $j < $this->rows; $j++) {
            for ($i = 0; $i < $this->rows; $i++) {
                if ($i !== $j) {
                    $const = $matrix[$i][$j] / $matrix[$j][$j];
                    for ($k = 0; $k <= $this->rows; $k++) {
                        $matrix[$i][$k] = round($matrix[$i][$k] - $const * $matrix[$j][$k], 2);
                    }
                }
            }
        }
        for ($i = 0; $i < $this->rows; $i++) {
            $solution[$i] = round($matrix[$i][$this->rows] / $matrix[$i][$i], 2);
        }
        $this->solution = $solution;
    }
}