<?php

namespace App\Libraries;

use Phpml\Math\Matrix;

class GaussElimination
{
    protected $matrix;
    protected $rows;
    protected $columns;
    protected $solution;
    protected $return_data;
    protected $matrix_obj;
    protected $b;

    public function __construct(Matrix $matrix)
    {
        $this->rows = $matrix->getRows();
        $this->columns = $matrix->getColumns();
        $this->matrix_obj = $matrix;
        $this->matrix = $this->getMatrices($matrix->toArray());
    }

    protected function getMatrices($matrix) 
    {
        if ($this->columns - $this->rows == 1) {
            for ($i = 0; $i < $this->rows; $i++){
                for ($j = 0; $j < $this->columns-1; $j++) {
                    if ($j == $this->columns - 2)
                        $this->b[$i] = $matrix[$i][$this->columns-1];
                }
            }
        }
        return $matrix;
    }

    public function handle()
    {
        $this->upperTriangulation($this->matrix);
        $this->backwardSubstitution($this->matrix);

        return $this->return_data = [
            'solution'  => $this->solution,
            'swapped'   => $this->round($this->matrix, $this->rows, $this->columns), 
            'input'     => $this->matrix_obj->toArray()
        ];
    }

    protected function upperTriangulation(array $matrix)
    {
        for ($j = 0; $j < $this->rows; $j++) {
            for ($i = 0; $i < $this->rows; $i++) {
                $maxPivot = $i;
                for ($k = $i + 1; $k < $this->rows; $k++) {
                    if (abs($matrix[$k][$i]) > abs($matrix[$i][$i]))
                        $maxPivot = $k;
                }
                if ($maxPivot !== $i) {
                    
                    if ($this->columns-$this->rows = 1) {
                        for ($k = $i; $k <= $this->rows; $k++) {
                            $temp = $matrix[$i][$k];
                            $matrix[$i][$k] = $matrix[$maxPivot][$k];
                            $matrix[$maxPivot][$k] = $temp;
                        }
                    } else {
                        for ($k = $i; $k < $this->rows; $k++) {
                            $temp = $matrix[$i][$k];
                            $matrix[$i][$k] = $matrix[$maxPivot][$k];
                            $matrix[$maxPivot][$k] = $temp;
                        }
                    }
                }
                if ($i > $j) {
                    $const = $matrix[$i][$j] / $matrix[$j][$j];
                    for ($k = 0; $k <= $this->rows; $k++) {
                        $matrix[$i][$k] = $matrix[$i][$k] - $const * $matrix[$j][$k];
                    }
                }
            }
        }
        $this->matrix = $matrix;
    }

    protected function backwardSubstitution(array $matrix)
    {
        $helper[$this->rows - 1] = $matrix[$this->rows - 1][$this->rows] / $matrix[$this->rows - 1][$this->rows - 1];
        for ($i = $this->rows - 1; $i >= 0; $i--) {
            $sum = 0;
            for ($j = $i + 1; $j < $this->rows; $j++) {
                $sum += $matrix[$i][$j] * $helper[$j];
            }
            $helper[$i] = ($matrix[$i][$this->rows] - $sum) / $matrix[$i][$i];
        }
        for ($i = 0; $i < $this->rows; $i++) {
            $this->solution[$i] = $helper[$i];
        }
    }

    protected function round(array $matrix, int $n, int $m)
    {
        for ($i = 0; $i < $n; $i++)
            for ($j = 0; $j < $m; $j++)
                $matrix[$i][$j] = round($matrix[$i][$j], 2);
        return $matrix;
    }
}