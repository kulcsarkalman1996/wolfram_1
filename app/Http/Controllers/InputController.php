<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Phpml\Math\Matrix;
use Illuminate\Support\Facades\Session;

class InputController extends Controller
{
    public static function getFileInput($file)
    {
        $destinationPath = 'uploads';
        $file = $file->move($destinationPath,$file->getClientOriginalName());
        foreach (file($file) as $key => $line) {
            $numbers = explode(" ", $line);
            $integers = [];
            foreach ($numbers as $number) {
                $number = (float)$number;
                $integers[] = $number;
            }
            $twoDarray[] = $integers;
        }
        $matrix = self::round($twoDarray);
        return $matrix;
    }

    public static function getInput(array $data)
    {
        for ($i = 0; $i < sizeof($data); $i++) {
            $indexed[$i] = $data[$i+1];
        }
        $matrix = self::round($indexed);
        return $matrix;
    }

    public static function round(array $matrix)
    {
        foreach ($matrix as $col => $line) {
            foreach ($line as $row => $item) {
                $matrix[$col][$row] = round($item, 2);
            }
        }
        return new Matrix($matrix);
    }
}
