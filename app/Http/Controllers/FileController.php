<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Phpml\Math\Matrix;
use App\Libraries\GaussElimination;
use App\Libraries\GaussJordan;
// use Phpml\Math\LinearAlgebra\LUDecomposition;
use App\Libraries\LUdecomposition;
use App\Libraries\CholeskyDecomposition;
use App\Libraries\LUPdecomposition;

class FileController extends Controller
{
    public function processFile(Request $request)
    {
        $file = $request->file('file');
        $destinationPath = 'uploads';
        $file = $file->move($destinationPath,$file->getClientOriginalName());
        foreach (file($file) as $key => $line) {
            $numbers = explode(" ", $line);
            $integers = [];
            foreach ($numbers as $number) {
                $number = (float)$number;
                $integers[] = $number;
            }
            $twoDarray[] = $integers;
        }
        $matrix = new Matrix($twoDarray);

        // $solution = (new GaussElimination($matrix))->handle();
        // $solution = (new GaussJordan($matrix))->handle();
        // $solution = (new LUdecomposition($matrix))->handle();
        $solution = (new LUPdecomposition($matrix))->handle();
        // $solution = (new CholeskyDecomposition($matrix))->handle();

        dd($solution);
    }
}
