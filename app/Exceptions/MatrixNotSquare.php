<?php

namespace App\Exceptions;

use Exception;

class MatrixNotSquare extends Exception
{
    protected $message = 'Matrix not square!';
}
