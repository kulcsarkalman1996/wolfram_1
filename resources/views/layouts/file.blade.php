{{'Select the file to upload'}}
{{ Form::file('file')}}
<div class="mt-2">
    Press {{ Form::submit('Upload File')}} to process the file
</div>