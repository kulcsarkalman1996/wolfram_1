<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/cholesky', function() {
    return view('cholesky');
})->name('cholesky.show');

Route::get('/gauss-elim', function() {
    return view('gauss');
})->name('gauss.show');

Route::get('/gauss-jordan', function() {
    return view('gauss_jordan');
})->name('gauss_jordan.show');

Route::get('/lu-decomposition', function() {
    return view('lu_decomposition');
})->name('lu_decomposition.show');

Route::get('/lup-decomposition', function() {
    return view('lup_decomposition');
})->name('lup_decomposition.show');

Route::post('/cholesky', 'CalcController@cholesky')->name('cholesky.calc');
Route::post('/gauss-elim', 'CalcController@gaussElimination')->name('gauss-elim.calc');
Route::post('/gauss-jordan', 'CalcController@gaussJordan')->name('gauss-jordan.calc');
Route::post('/lu-decomposition', 'CalcController@luDecomposition')->name('lu-decomposition.calc');
Route::post('/lup-decomposition', 'CalcController@lupDecomposition')->name('lup-decomposition.calc');

Route::post('/det', 'FileController@processFile');


